﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataAnalyzer
{
    public class DataManager
    {
        private string _fileName;

        public DataManager(string fileName)
        {
            _fileName = fileName;
        }

        public List<Person> ReadFromFile()
        {
            var persons = new List<Person>();
            using (var reader = new StreamReader(_fileName))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    var person = new Person
                    {
                        Id = Convert.ToInt32(values[0]),
                        FirstName = values[1],
                        LastName = values[2],
                        Email = values[3],
                        Department= values[4],
                        StartEmploymentDate = DateTime.Parse(values[5])
                    };
                    persons.Add(person);
                }
            }
            return persons;
        }

        public List<Person> GetByFirstName(string name)
        {
            var persons = ReadFromFile();
            return persons.FindAll(p => p.FirstName == name);
        }
        public List<Person> GetByLastName(string lastName)
        {
            var persons = ReadFromFile();
            return persons.FindAll(p => p.LastName == lastName);
        }

        public List<Person> GetByDepartment(string department)
        {
            var persons = ReadFromFile();
            return persons.FindAll(p => p.Department == department);
        }

        public Person GetById(int id)
        {
            var persons = ReadFromFile();
            var person = persons.FirstOrDefault(p => p.Id == id);
            return person;
        }

        public int GetNumberOfVacationDays(int id) {
            var employee = GetById(id);
            var numberOfYearsInCompany = DateTime.Now.Year - employee.StartEmploymentDate.Year;

            if (numberOfYearsInCompany < 5)
            {
                return 23;
            }
            else if (numberOfYearsInCompany >= 5 && numberOfYearsInCompany < 10)
            {
                return 26;
            }
            else {
                return 29;
            }

        }

        public bool IsAllowedWorkFromHome(int id)
        {
            var employee = GetById(id);
            var numberOfYearsInCompany = DateTime.Now.Year - employee.StartEmploymentDate.Year;

            if (numberOfYearsInCompany < 2)
            {
                return false;
            }
            return true;
        }
        public void Notify(Person person, string message)
        {
            Notifier notifier = new Notifier();
            notifier.SendNotification(person.Email, message);
        }

        public void NotifyDepartmant(string departmantName)
        {
            Notifier notifier = new Notifier();
            var persons = GetByDepartment(departmantName);
            notifier.SendNotificationToGroup(persons, $"Notification for {departmantName}");
        }
    }
}
