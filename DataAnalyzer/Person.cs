﻿using System;

namespace DataAnalyzer
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public DateTime StartEmploymentDate { get; set; }
    }
}
