﻿using System;

namespace DataAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            DataManager dataManager = new DataManager("Data//Persons.csv");            

            var people = dataManager.GetByDepartment("Accounting");

            foreach (var person in people)
            {
                var message = $"{person.FirstName} {person.LastName} has {dataManager.GetNumberOfVacationDays(person.Id)} vacation days and " + (dataManager.IsAllowedWorkFromHome(person.Id) ? "is " : "in not ") + "allowed to work from home.";
                dataManager.Notify(person, message);
            }
        }
    }
}
