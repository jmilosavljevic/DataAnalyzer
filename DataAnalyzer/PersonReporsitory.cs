﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAnalyzer
{
    public class PersonReporsitory
    {
        private List<Person> _persons;
        public void Save(List<Person> persons) {
            _persons = persons;
        }
        public List<Person> GetByFirstName(string name)
        {
            return _persons.Where(p => p.FirstName == name).ToList();
        }
        public List<Person> GetByLastName(string lastName)
        {
            return _persons.Where(p => p.LastName == lastName).ToList(); ;
        }

        public List<Person> GetByDepartment(string department)
        {
            return _persons.Where(p => p.Department == department).ToList();
        }

        public List<Person> GetBySenority(string senority)
        {
            return _persons.Where(p => p.Senority == senority).ToList(); ;
        }
    }
}
