﻿using System;
using System.Collections.Generic;

namespace DataAnalyzer
{
    public class Notifier
    {
        public void SendNotification(string email, string message) {
           Console.WriteLine($"Notification email sent to {email}: {message}");
        }

        public void SendNotificationToGroup(List<Person> persons, string message)
        {
            foreach (var person in persons)
            {
                SendNotification(person.Email, message);
            }
        }
    }
}
